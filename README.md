# Readme

## Lighthouse

### Resources

- https://www.smashingmagazine.com/2020/05/performance-visible-hoodoo-gitlab-artifacts/
- https://medium.com/weekly-webtips/web-performance-integrating-lightouse-ci-in-your-gitlab-ci-cd-pipeline-129c788591bd
- https://blog.akansh.com/integrate-lighthouse-ci-with-static-site-generators/
- https://www.npmjs.com/package/lighthouse-ci

### Methodology

Main idea: MR can decrease any Lighthouse score (Performance, Accessibility, Best Practices, SEO) by 5% but not under 80%. Create a pipeline job to check this.

### STEP 1

Use NPM to create a report. Then it can by called locally by developer.
