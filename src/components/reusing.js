import React from "react"
import * as Styled from "./styles"


const Layout = () => {
	return (
		<div className="global-wrapper">
			<Styled.Setup13>Reusing testing page</Styled.Setup13>
			<Styled.Setup01>
				<h2 className="h2">
					Styled.Setup01
				</h2>
				Styled.Setup01
			</Styled.Setup01>
			<Styled.Setup02>
				<h2 className="h2">
					Styled.Setup02
				</h2>
				Styled.Setup02
			</Styled.Setup02>
			<hr/>
			<Styled.Setup1>
				<h2 className="h2">
					Styled.Setup1
				</h2>
				Styled.Setup1
			</Styled.Setup1>
			<Styled.Setup2>
				<h2 className="h2">
					Styled.Setup2
				</h2>
				Styled.Setup2
			</Styled.Setup2>
			<hr/>
			<Styled.Setup11>
				<Styled.Setup13>
					Styled.Setup11
				</Styled.Setup13>
				Styled.Setup11
			</Styled.Setup11>
			<Styled.Setup12>
				<Styled.Setup13>
					Styled.Setup12
				</Styled.Setup13>
				Styled.Setup12
			</Styled.Setup12>
			<hr />
			<Styled.Setup21>
				<Styled.Setup23>
					Styled.Setup21
				</Styled.Setup23>
				Styled.Setup21
			</Styled.Setup21>
			<Styled.Setup22>
				<Styled.Setup23>
					Styled.Setup22
				</Styled.Setup23>
				Styled.Setup22
			</Styled.Setup22>
			<hr />
			<Styled.Setup31>
				<Styled.Setup33>
					Styled.Setup31
				</Styled.Setup33>
				Styled.Setup31
			</Styled.Setup31>
			<Styled.Setup32>
				<Styled.Setup33>
					Styled.Setup32
				</Styled.Setup33>
				Styled.Setup32
			</Styled.Setup32>
		</div>
	)
}

export default Layout
