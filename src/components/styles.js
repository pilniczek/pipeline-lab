import { styled } from "linaria/react"

// #0

// Tom's way

export const stringInterpolation = "color: red;"

export const Setup01 = styled.div`
	color: blue;
	.h2 {
		${stringInterpolation}
	}
`
export const Setup02 = styled.div`
	color: orange;
	.h2 {
		${stringInterpolation}
	}
`

// change request: change headline color in Setup2
// change request: change headline tag in Setup2

// #1

// Tom's way

export const Setup1 = styled.div`
	color: blue;
	.h2 {
		color: red;
	}
`
export const Setup2 = styled.div`
	color: orange;
	.h2 {
		color: red;
	}
`

// change request: change headline color in Setup2
// change request: change headline tag in Setup2

// break

// #2

// The Mára's way

export const Setup11 = styled.div`
	color: blue;
`
export const Setup12 = styled.div`
	color: orange;
`
export const Setup13 = styled.h2`
	color: grey;
`

// change request: change headline color in Setup12
// change request: change headline tag in Setup12

// break

// #3

// utils from linaria documentation
// Ted's way

export const Setup21 = styled.div`
	color: blue;
`
export const Setup22 = styled.div`
	color: orange;
`
export const Setup23 = styled(Setup21)`
	color: red;
`

// change request: change headline color in Setup12
// change request: change headline tag in Setup12

// use this as component--variant from BEM

// break

// #4

// utils from linaria documentation
// Ted's way

export const Setup33 = styled.h2`
	color: cyan;
`
export const Setup31 = styled.div`
	color: blue;
	${Setup33} {
		color: red;
	}
`
export const Setup32 = styled.div`
	color: orange;
	${Setup33} {
		color: red;
	}
`

// change request: change headline color in Setup12
// change request: change headline tag in Setup12

// break
