module.exports = {
	ci: {
		collect: {
			numberOfRuns: 5,
			startServerCommand: 'npm run serve',
			url: ['http://localhost:9000/'],
			settings: {
				// exclude "pwa" category
				onlyCategories: [
					'performance',
					'accessibility',
					'best-practices',
					'seo',
				],
				chromeFlags: '--no-sandbox',
				extraHeaders: JSON.stringify({
					Cookie: 'customCookie=1;foo=bar',
				}),
			},
		},
		assert: {
			assertions: {
				'categories:performance': [
					'warn',
					{ minScore: 0.9, aggregationMethod: 'median-run' },
				],
				'categories:accessibility': [
					'warn',
					{ minScore: 1, aggregationMethod: 'pessimistic' },
				],
				'categories:best-practices': [
					'warn',
					{ minScore: 1, aggregationMethod: 'pessimistic' },
				],
				'categories:seo': [
					'warn',
					{ minScore: 1, aggregationMethod: 'pessimistic' },
				],
			},
		},
		upload: {
			target: 'temporary-public-storage',
		},
	},
}
